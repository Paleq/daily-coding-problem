import UnivalTreeCount._
import org.scalatest.WordSpec

class UnivalTreeCountTest extends WordSpec {

  "Unival tree count" when {
    "a tree containing 4 nodes" should {
      "return 4" in {
        val input = TreeNode(0,
          Some(TreeNode(1,
            None,
            None
          )),
          Some(TreeNode(0,
            Some(TreeNode(1,
              Some(TreeNode(1,
                None,
                None
              )),
              Some(TreeNode(1,
                None,
                None
              ))
            )),
            None
          ))
        )

        val expected = 4

        assert(countTreeNodes(input) == expected)
      }
    }
  }

}
