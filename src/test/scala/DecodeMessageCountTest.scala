import org.scalatest.WordSpec
import DecodeMessageCount._

class DecodeMessageCountTest extends WordSpec {

  "The DecodeMessageCount" when {
    "given the message '111'" should {
      "return 3" in {
        assert(countPermutations("111", 3) == 3)
      }
    }
  }

}
