import org.scalatest.WordSpec
import ProductOfNumbersExceptIndex._

class ProductOfNumbersExceptIndexTest extends WordSpec {

  "Product of numbers except the current number" when {
    "provided with List(1, 2, 3, 4, 5)" should {
      "return List(120, 60, 40, 30, 24)" in {
        val input = List(1, 2, 3, 4, 5)
        val expected = List(120, 60, 40, 30, 24)

        assert(product(input) === expected)
      }
    }
  }

}
