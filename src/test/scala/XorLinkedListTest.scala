import org.scalatest.WordSpec

class XorLinkedListTest extends WordSpec {

  "An XOR Linked list at the first position" when {
    "the next node is requested" should {
      "contain the string 'Simon'" in {
        val expected = "Simon"
        val head = new XorListNode("Hello")()
        val nextAddr = head.getAddress(head.nextPrev)

        head.insert(head, "Simon")

        assert(head.next(nextAddr).get.data == expected)
      }
    }

  }

}
