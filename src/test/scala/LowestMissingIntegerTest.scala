import org.scalatest.WordSpec
import LowestMissingInteger._

class LowestMissingIntegerTest extends WordSpec {

  "Lowest missing integer" when {
    "provided with List(3, 4, -1, 1)" should {
      "return 2" in {
        val input = List(3, 4, -1, 1)
        val expected = 2

        assert(findLowestMissing(input) === expected)
      }
    }

    "provided with List(1, 2, 0)" should {
      "return 3" in {
        val input = List(1, 2, 0)
        val expected = 3

        assert(findLowestMissing(input) == expected)
      }
    }
  }

}
