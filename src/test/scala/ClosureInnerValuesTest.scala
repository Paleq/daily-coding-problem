import org.scalatest.WordSpec
import ClosureInnerValues._

class ClosureInnerValuesTest extends WordSpec {

  "Closure inner values" when {
    "called with car()" should {
      "return 3" in {
        val input = cons(3, 4)
        val expected = 3

        assert(car(input) == expected)
      }
    }

    "called with cdr()" should {
      "return 4" in {
        val input = cons(3, 4)
        val expected = 4

        assert(cdr(input) == expected)
      }
    }
  }

}
