import SumOfNonAdjacentNumbers._
import org.scalatest.WordSpec

class SumOfNonAdjacentNumbersTest extends WordSpec {

  "Sum of non-adjacent numbers" when {
    "applied to the list 2, 4, 6, 2, 5" should {
      "return 13" in {
        val expected = 13
        assert(sumOfNonAdjacentNumbers(List(2, 4, 6, 2, 5)) == expected)
      }
    }

    "applied to the list 5, 1, 1, 5" should {
      "return 10" in {
        val expected = 10
        assert(sumOfNonAdjacentNumbers(List(5, 1, 1, 5)) == expected)
      }
    }
  }
}
