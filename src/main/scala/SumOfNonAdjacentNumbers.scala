/*
  #9 Given a list of integers, write a function that returns the largest sum of non-adjacent numbers. Numbers can be 0 or negative.

  For example, [2, 4, 6, 2, 5] should return 13, since we pick 2, 6, and 5. [5, 1, 1, 5] should return 10, since we pick 5 and 5.

  Issued on 06 April 2019
 */

object SumOfNonAdjacentNumbers {

  def sumOfNonAdjacentNumbers(list: List[Int], acc: Int = 0, tmp: Int = 0): Int = {
    if (list.isEmpty) acc
    else sumOfNonAdjacentNumbers(list.drop(1), Math.max(tmp + list.head, acc), acc)
  }

}
