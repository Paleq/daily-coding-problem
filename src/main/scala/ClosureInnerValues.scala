/*
  #5 cons(a, b) constructs a pair, and car(pair) and cdr(pair) returns the first and last element of that pair.
  For example, car(cons(3, 4)) returns 3, and cdr(cons(3, 4)) returns 4.

  Issued on 02 April 2019
 */

object ClosureInnerValues {

  def cons(a: Int, b: Int) = {
    def pair(f: (Int, Int) => Int) = f(a,b)
    pair(_)
  }

  def car(f: ((Int, Int) => Int) => Int): Int = {
    val pair = f
    pair((a, _) => a)
  }


  def cdr(f: ((Int, Int) => Int) => Int): Int = {
    val pair = f
    pair((_, b) => b)
  }

}
