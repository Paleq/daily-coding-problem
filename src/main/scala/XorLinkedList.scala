/*
  #6 An XOR linked list is a more memory efficient doubly linked list.
  Instead of each node holding next and prev fields, it holds a field named both,
  which is an XOR of the next node and the previous node. Implement an XOR linked list;
  it has an add(element) which adds the element to the end, and a get(index) which returns the node at index.

  Issued on 03 Apr 2019
 */

import scala.annotation.tailrec
import scala.collection.mutable.HashMap
import scala.util.Random

class SimulatedMemory {
  lazy private val memory: HashMap[Byte, XorListNode] = HashMap()

  @tailrec
  final def allocateAddress(): Byte = {
    val address = Random.nextInt.toByte

    if (!memory.contains(address)) {
      setValue(address, new XorListNode("")())
      address
    }
    else allocateAddress()
  }

  def setValue(address: Byte, node: XorListNode) = memory.update(address, node)

  def getValueOpt(address: Byte): Option[XorListNode] = memory.get(address)
}

object SimulatedMemory extends SimulatedMemory

class XorListNode(val data: String, val nextPrev: Byte = 0)(memory: SimulatedMemory = SimulatedMemory) {

  def getAddress(address: Byte): Byte = (this.nextPrev ^ address).toByte

  def insert(headNode: XorListNode, data: String) = {
    val nextAddress = getAddress(memory.allocateAddress())
    memory.setValue(getAddress(nextAddress), new XorListNode(this.data, nextAddress)())
    memory.setValue(getAddress(0), new XorListNode(data)())
  }

  def next(address: Byte): Option[XorListNode] = memory.getValueOpt(getAddress(address))
}

