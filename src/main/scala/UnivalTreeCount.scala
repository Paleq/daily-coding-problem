import scala.annotation.tailrec

/*
  #8 A unival tree (which stands for "universal value") is a tree where all nodes under it have the same value.

  Given the root to a binary tree, count the number of unival subtrees.

  Issued on 05 Apr 2019
 */

case class TreeNode(value: Int, left: Option[TreeNode], right: Option[TreeNode])

object UnivalTreeCount {

  def countTreeNodes(tree: TreeNode, count: Int = 0): Int = {
    def getBranchValue(tree: Option[TreeNode]) = tree match {
      case Some(branch) => countTreeNodes(branch, count + branch.value)
      case None => 0
    }
    tree.value + getBranchValue(tree.left) + getBranchValue(tree.right)
  }

}
