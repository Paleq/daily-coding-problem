/*
  #2 Given an array of integers, return a new array such that each element at index i of the new array
  is the product of all the numbers in the original array except the one at i.

  Issued on 30 Mar 2019
 */

object ProductOfNumbersExceptIndex {

  def product(list: List[Int]) = {
    for {
      elem <- list
      product = list.diff(List(elem)).reduce(_ * _)
    }
    yield product
  }

}
