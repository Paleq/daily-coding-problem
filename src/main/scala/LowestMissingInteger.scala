/*
  #4 Given an array of integers, find the first missing positive integer in linear time and constant space.

  Issued on 01 April 2019
 */

object LowestMissingInteger {

  def findLowestMissing(list: List[Int]) = {
    val sortedList = list.sorted.filter(_ > -1)

    (sortedList.head to sortedList.last)
      .diff(sortedList)
      .headOption
      .getOrElse(sortedList.last + 1)
  }

}
