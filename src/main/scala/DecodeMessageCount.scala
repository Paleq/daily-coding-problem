import scala.annotation.tailrec

/*
  #7 Given the mapping a = 1, b = 2, ... z = 26, and an encoded message, count the number of ways it can be decoded.
  For example, the message '111' would give 3, since it could be decoded as 'aaa', 'ka', and 'ak'.

  Issued on 04 Apr 2019
 */

object DecodeMessageCount {

  @tailrec
  def countPermutations(message: String, n: Int, count: Int = 0): Int = {
    if (n < 1) count
    else {
      val lastDigits = message.take(n).takeRight(2).toInt
      if (lastDigits > 0 && lastDigits <= 26) countPermutations(message, n - 1, count + 1)
      else count
    }
  }

}
